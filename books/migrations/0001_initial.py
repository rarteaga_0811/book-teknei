# Generated by Django 4.1.4 on 2023-03-07 06:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Autor',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=180)),
                ('nacionalidad', models.CharField(choices=[('M', 'Mexicano'), ('E', 'Español'), ('O', 'Otro')], max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Books',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField(auto_created=True, auto_now_add=True)),
                ('titulo', models.CharField(max_length=150)),
                ('titulo_ingles', models.CharField(blank=True, max_length=150, null=True)),
                ('sinopsis', models.TextField(max_length=255)),
                ('sinopsis_ingles', models.TextField(blank=True, max_length=255, null=True)),
                ('is_Active', models.BooleanField(default=True)),
                ('portada', models.ImageField(upload_to='static/images')),
                ('autor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='books.autor')),
            ],
            options={
                'verbose_name_plural': 'Books',
            },
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('categoria', models.CharField(max_length=180)),
                ('is_Active', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='BooksCategory',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('book', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='books.books')),
                ('category', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='books.category')),
            ],
        ),
    ]
