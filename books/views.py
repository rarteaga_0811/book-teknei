#django
from django.shortcuts import render, redirect
from django.views.generic import ListView
from django.contrib.auth.decorators import login_required

#django rest framework
from rest_framework.generics import ListAPIView, CreateAPIView, UpdateAPIView, RetrieveUpdateAPIView
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

#local models
from .models import Books, BooksCategory, Autor, Category
from users.models import User, LengauageUser

#services
from .services.bookServices import BookServices

from .serializers import (
    BookSerializers, 
    BookCategorySerializers,
    BookCreateCategorySerializers,
    CreateBookSerializers,
    ListCategorySerializers,
    CreateCategorySerializers,
    ListAutorSerializer,
    UpdateBookSerializers,
    UpdateCategorySerializers,
    UpdateAutorSerializer,
    BookPagination)

#Templates
@login_required
def autorCreate(request):
    data = {}
    if request.method == "POST":
        booksobj = BookServices()
        language = LengauageUser.objects.get(user = request.user)
        nombre = request.POST['nombreautor']
        nacionalidad = request.POST['nacionalidad']
        autorCreated = Autor.objects.create(
            nombre =nombre,
            nacionalidad = nacionalidad

        )
        books = booksobj.getBooklist()
        data = {'books':books,
               'language': language.language,
               'autorcreado':autorCreated,}
        return render(request,"books/home.html",data)
    #/books/create-autor"
    return render(request,"books/creaautor.html",data)

@login_required
def createBookCategory(request):
    booksobj = BookServices()
    books = booksobj.getBooklist()
    category = Category.objects.all()
    data = {'books':books,
            'categorias':category}
    if request.method == "POST":
        libro = request.POST['book']
        categoria = request.POST['category']
        bookObj = Books.objects.get(id=libro)
        categoObj = Category.objects.get(id = categoria)
        print(libro)
        print(categoria)
        BooksCategory.objects.create(book=bookObj,category=categoObj)
        language = LengauageUser.objects.get(user = request.user)
        data = {'books':books,
                'categorias':category,
                'creado':'1'}
        return render(request,"books/book-category.html",data)

    #/books/book-category
    return render(request,"books/book-category.html",data)

@login_required
def bookCreate(request):
    autores = Autor.objects.all()
    data ={'autores':autores,}
    if request.method == "POST":
        booksobj = BookServices()
        language = LengauageUser.objects.get(user = request.user)
        #From post rquest html
        titulo = request.POST['titulo']
        titulo_ingles = request.POST['tituloingles']
        sinopsis = request.POST['sinopsis']
        sinopsis_ingles = request.POST['sinopsisingles']
        autor = request.POST['autor']
        portada = request.POST['FormControlImage64']
        databook = {
            "titulo": titulo,
            "titulo_ingles":titulo_ingles,
            "sinopsis":sinopsis,
            "sinopsis_ingles":sinopsis_ingles,
            "autor":autor,
            "portada":portada
        }
        booksCreated = booksobj.createBook_api(databook)
        books = booksobj.getBooklist()
        autores = Autor.objects.all()
        data = {'books':books,
               'language': language.language,
               'libroCreado':booksCreated,}
        return render(request,"books/home.html",data)

    return render(request,"books/createBook.html",data)

@login_required
def bookView(request):
    booksobj = BookServices()
    books = booksobj.getBooklist()
    language = LengauageUser.objects.get(user = request.user)
    data = {'books':books,
            'language': language.language}
    return render(request,"books/home.html",data)

@login_required
def bookView_ByID(request, book_id):
    categorias = ""
    booksobj = BookServices()
    books = booksobj.getBook_byID(book_id)
    bookCate = booksobj.getCategoryBook_byID(book_id)
    for bookCate in bookCate:
        if categorias =="":
            categorias = bookCate['category']['categoria']
        else:
            categorias = categorias +', ' + bookCate['category']['categoria']
    language = LengauageUser.objects.get(user = request.user)
    data = {'books':books,
            'language': language.language,
            'categorys': categorias,
    }
    return render(request,"books/bookDescription.html",data)

@login_required
def setLanguage(request,langauge):
    booksobj = BookServices()
    bookslan = booksobj.setLanguage(langauge)
    return redirect('books:home')



### Views Rest Framework ###
class ListBookAPI(ListAPIView):
    """ Lista todos los libros /all, o por id /[0-9] """
    #serializer
    serializer_class = BookSerializers
    authentication_classes = (TokenAuthentication,)
    permission_classes = [IsAuthenticated,]

    def get_queryset(self):
        #usuario = self.request.user
        if self.kwargs['libro'] in ('all','ALL','All','0','null') :
            lista = Books.objects.all().order_by('-date_created')
        else:
            id_libro = self.kwargs['libro']
            lista = Books.objects.filter(id = int(id_libro), is_Active = True)
        return lista

class ListCategoryByBook(ListAPIView):
    """ Categorias por libro """
    serializer_class = BookCategorySerializers
    authentication_classes = (TokenAuthentication,)
    permission_classes = [IsAuthenticated,]

    def get_queryset(self):
        libro = self.kwargs['libro']
        lista = BooksCategory.objects.filter(book_id = int(libro))
        return lista

class ListCategoryBook(ListAPIView):
    """ Categorias y libros (Todo) """
    serializer_class = BookCategorySerializers
    authentication_classes = (TokenAuthentication,)
    permission_classes = [IsAuthenticated,]

    def get_queryset(self):
        lista = BooksCategory.objects.all()
        return lista

class ListCategory(ListAPIView):
    """Lista de categorias """
    serializer_class = ListCategorySerializers
    authentication_classes = (TokenAuthentication,)
    permission_classes = [IsAuthenticated,]

    def get_queryset(self):
        lista = Category.objects.all()
        return lista

class ListAutor(ListAPIView):
    """ Lista de autores ---- /{0-9}, /all, /null"""
    serializer_class = ListAutorSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = [IsAuthenticated,]

    def get_queryset(self):
        if self.kwargs['autor'] in ('all','ALL','All','0','null') :
            lista = Autor.objects.all()
        else:
            autor = self.kwargs['autor']
            lista = Autor.objects.filter(id = int(autor)) 
        
        return lista

class CreateBookAPI(CreateAPIView):
    """ Crear Libro """
    #serializer
    serializer_class = CreateBookSerializers
    authentication_classes = (TokenAuthentication,)
    permission_classes = [IsAuthenticated,]

class CreateCategoryBook(CreateAPIView):
    """ Crear Categoria para libro """
    serializer_class = BookCreateCategorySerializers
    authentication_classes = (TokenAuthentication,)
    permission_classes = [IsAuthenticated,]

class CreateCategory(CreateAPIView):
    """ Crear nueva categoria """
    serializer_class = CreateCategorySerializers
    authentication_classes = (TokenAuthentication,)
    permission_classes = [IsAuthenticated,]


class CreateAutor(CreateAPIView):
    """ Crear nuevo Autor Nacionalidad: E - Español / M - Mexicano / O - Otro  """
    serializer_class = ListAutorSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = [IsAuthenticated,]

class UpdateBook(UpdateAPIView):
    """ Actualizar datos del un libro - NA """
    authentication_classes = (TokenAuthentication,)
    permission_classes = [IsAuthenticated,]
    serializer_class = UpdateAutorSerializer
    queryset = Books.objects.all()
    

class ModificarBook(RetrieveUpdateAPIView):
    """ Modificar datos del un libro"""
    authentication_classes = (TokenAuthentication,)
    permission_classes = [IsAuthenticated,]
    serializer_class = UpdateBookSerializers
    queryset = Books.objects.all()

class ModificarCategoria(RetrieveUpdateAPIView):
    """Modificar Categoria"""
    authentication_classes = (TokenAuthentication,)
    permission_classes = [IsAuthenticated,]
    serializer_class = UpdateCategorySerializers
    queryset = Category.objects.all()

class ModifcarAutor(RetrieveUpdateAPIView):
    """ Modifcar Autor """
    authentication_classes = (TokenAuthentication,)
    permission_classes = [IsAuthenticated,]
    serializer_class = ListAutorSerializer
    queryset = Autor.objects.all()

