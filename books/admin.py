#Django
from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.models import Group

#Local models
from .models import Category, Autor, Books, Autor, BooksCategory


#Admin
class AutorAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'nacionalidad',]

class BookAdmin(admin.ModelAdmin):
    list_display = ['titulo']

class CategoryAdmin(admin.ModelAdmin):
    list_display = ['categoria','is_Active']

class BookCategoryAdmin(admin.ModelAdmin):
    list_display = ['book','category']

#Register
admin.site.register(Category, CategoryAdmin)
admin.site.register(Autor, AutorAdmin)
admin.site.register(Books, BookAdmin)
admin.site.register(BooksCategory, BookCategoryAdmin)

#Unregister
#admin.site.unregister(User)
#admin.site.unregister(Group)