""" Books Urls"""
from django.urls import path


#local models
from .views import (
    ListBookAPI, 
    CreateBookAPI,
    CreateCategoryBook,
    CreateCategory,
    ListCategory,
    ListAutor,
    CreateAutor,
    ModificarBook,
    ModificarCategoria,
    ModifcarAutor,
    ListCategoryByBook,
    #templates
    bookView,
    bookView_ByID,
    setLanguage,
    bookCreate,
    autorCreate,
    createBookCategory)

urlpatterns = [
     path(route ='books/api/books/list/<libro>', view = ListBookAPI.as_view(), name='APIListbook'),
     path(route ='books/api/books/create', view = CreateBookAPI.as_view(), name='APICreatebook'),
     path(route ='books/api/books/update/<pk>', view = ModificarBook.as_view(), name='APIUpdatebook'),
     path(route ='books/api/books/create-category', view = CreateCategoryBook.as_view(), name='APICreatebookCategory'),
     path(route ='books/api/books/list-categorysbybook/<libro>', view = ListCategoryByBook.as_view(), name='APICreatebookCategory'),
     path(route ='category/api/create', view = CreateCategory.as_view(), name='APICreatebookCategory'),
     path(route ='category/api/list', view = ListCategory.as_view(), name='APIListCategory'),
     path(route ='category/api/update/<pk>', view = ModificarCategoria.as_view(), name='APIUpdatebook'),
     path(route ='autor/api/autor/list/<autor>', view = ListAutor.as_view(), name='APIListAutor'),
     path(route ='autor/api/create', view = CreateAutor.as_view(), name='APICreateAutor'),
     path(route ='autor/api/update/<pk>', view = ModifcarAutor.as_view(), name='APICreateAutor'),
     #Templates
     path(route = 'home/', view = bookView,name='home'),
     path(route = 'book-description/<book_id>', view = bookView_ByID, name ='bookDescription'),
     path(route = 'language/<langauge>', view = setLanguage, name="userlanguage"),
     path(route = 'books/create', view = bookCreate, name = 'bookcreate'),
     path(route = 'books/create-autor', view = autorCreate, name ='create-autor'),
     path(route = 'books/book-category', view = createBookCategory,  name = 'create-category'),
]