#rest framework
from rest_framework import serializers, pagination
#drf extra fields
from drf_extra_fields.fields import Base64ImageField

#local models
from .models import Books, BooksCategory, Autor, Category

class ListAutorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Autor
        fields =(
            'id',
            'nombre',
            'nacionalidad',
        )

class ListCategorySerializers(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = (
            'id',
            'categoria',
            'is_Active'
        )

#Categoria-Libro
class BookCategorySerializers(serializers.ModelSerializer):
    category = ListCategorySerializers()
    class Meta:
        model = BooksCategory
        fields = (
            'category',
        )

class BookSerializers(serializers.ModelSerializer):
    """ Listado de libros """
    autor = ListAutorSerializer()
    class Meta:
        model = Books
        fields = (
            'id',
            'titulo',
            'titulo_ingles',
            'sinopsis',
            'sinopsis_ingles',
            'autor',
            'is_Active',
            'portada', 
            'date_created',
        )

#Pagination books
class BookPagination(pagination.PageNumberPagination):
    page_size = 4
    max_page_size = 100

class UpdateBookSerializers(serializers.ModelSerializer):
    class Meta:
        model = Books
        fields = (
            'titulo',
            'sinopsis',
            'autor',
            'is_Active',
            'portada',
        )

class CreateBookSerializers(serializers.ModelSerializer):
    portada = Base64ImageField()
    class Meta:
        model = Books
        fields = (
            'titulo',
            'titulo_ingles',
            'sinopsis',
            'sinopsis_ingles',
            'autor',
            'portada', 
        )



class UpdateAutorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Autor
        fields =(
            'nombre',
            'nacionalidad',
        )


class BookCreateCategorySerializers(serializers.ModelSerializer):
    #category = ListCategorySerializers()
    class Meta:
        model = BooksCategory
        fields = (
            'book',
            'category',
        )



class UpdateCategorySerializers(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = (
            'categoria',
            'is_Active'
        )

class CreateCategorySerializers(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = (
            'categoria',
        )
