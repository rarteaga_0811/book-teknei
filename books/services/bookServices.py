import json
import time

import requests


class BookServices(object):
    """ Consumir servicios de book API """
    URL_BASE = 'http://localhost:8000/'
    BASE_HEADER = {
        'Content-Type': 'application/json',
        'Authorization': 'Token 1d662b1db5aba86c3d28dabaffd95f164982f6e8'
    }

    def setLanguage(self, language):
        url = self.URL_BASE + 'user-language'
        payload = {
            "language": language
         }
        result = requests.request(
            "POST",
            url=url,
            headers=self.BASE_HEADER,
            data=json.dumps(payload)
        )
        response = result.json()
        return response

    def getBooklist(self):
        url = self.URL_BASE + 'books/api/books/list/all'
        result = requests.request(
            "GET",
            url=url,
            headers=self.BASE_HEADER,
        )
        response = result.json()
        return response
    
    def getBook_byID(self,book_id):
        url = self.URL_BASE + 'books/api/books/list/' + str(book_id)
        result = requests.request(
            "GET",
            url=url,
            headers=self.BASE_HEADER,
        )
        response = result.json()
        return response
    
    def getCategoryBook_byID(self,book_id):
        url = self.URL_BASE + 'books/api/books/list-categorysbybook/' + str(book_id)
        result = requests.request(
            "GET",
            url=url,
            headers=self.BASE_HEADER,
        )
        response = result.json()
        return response
    
    def crerateBook_byID(self,book_id):
        url = self.URL_BASE + 'books/api/books/list/' + str(book_id)
        result = requests.request(
            "GET",
            url=url,
            headers=self.BASE_HEADER,
        )
        response = result.json()
        return response

    def createBook_api(self, book):
        url = self.URL_BASE + 'books/api/books/create' 
        payload = {
            "titulo": book["titulo"],
            "titulo_ingles":book["titulo_ingles"],
            "sinopsis":book["sinopsis"],
            "sinopsis_ingles":book["sinopsis_ingles"],
            "autor":book["autor"],
            "portada": book["portada"]
         }
        print(payload)
        print("POST Create book")
        result = requests.request(
            "POST",
            url=url,
            headers=self.BASE_HEADER,
            data=json.dumps(payload)
        )
        response = result.json()
        print(response)
        return response



