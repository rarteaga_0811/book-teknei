from django.db import models

#Local models
from .autor import Autor
from .category import Category


#
class Books(models.Model):
    """
    Modelo principal para libros
    """
    titulo = models.CharField(max_length=150)
    titulo_ingles = models.CharField(max_length=150, blank = True, null= True)
    sinopsis = models.TextField(max_length=255)
    sinopsis_ingles = models.TextField(max_length=255, blank = True, null= True)
    autor = models.ForeignKey(Autor,on_delete=models.CASCADE)
    is_Active = models.BooleanField(default=True)
    portada = models.ImageField(upload_to='static/images')
    date_created = models.DateTimeField(auto_created=True, auto_now_add=True)

    class Meta:
        verbose_name_plural = "Books"

    def __str__(self):
        return self.titulo

class BooksCategory(models.Model):
    """
    Modelo relacional libros - Categorias
    """
    book = models.ForeignKey(Books, on_delete=models.CASCADE, null=True, blank=True) 
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        #return str(self.book + '-' + self.category)
        return str(self.book)