#Django models
from django.db import models

#Local models


#
class Category(models.Model):
    """
    Modelo principal de Categorias
    """
    categoria = models.CharField(max_length=180)
    is_Active = models.BooleanField(default=True)

    def __str__(self):
        return self.categoria