#Django models
from django.db import models
from django.utils.translation import gettext_lazy as _



class Autor(models.Model):
    """
    Modelo principal de Autores
    """
    class AutorNacionalidad(models.TextChoices):
        MEXICO = 'M', _('Mexicano')
        ESPAÑA = 'E', _('Español')
        OTRO = 'O',_('Otro')
    nombre = models.CharField(max_length=180)
    nacionalidad = models.CharField(
        max_length=30,
        choices=AutorNacionalidad.choices
    )


    def __str__(self):
        return str(self.nombre)