from django.db import models

from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
#
from .managers import UserManager

class User(AbstractBaseUser, PermissionsMixin):

    GENDER_CHOICES = (
        ('M', 'Masculino'),
        ('F', 'Femenino'),
        ('O', 'Otros'),
    )

    email = models.EmailField(unique=True)
    full_name = models.CharField('Nombres', max_length=100)
    city = models.CharField(
        'Ocupacion',
        max_length=30, 
        blank=True
    )
    genero = models.CharField(
        max_length=1, 
        choices=GENDER_CHOICES, 
        blank=True
    )
    date_birth = models.DateField(
        'Fecha de nacimiento', 
        blank=True,
        null=True
    )
    #
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)

    USERNAME_FIELD = 'email'

    REQUIRED_FIELDS = ['full_name']

    objects = UserManager()

    def get_short_name(self):
        return self.email
    
    def get_full_name(self):
        return self.full_name
    

class LengauageUser(models.Model):
    LANGUAGE_CHOICES = (
        ('S', 'Spanish'),
        ('E', 'English'),
    )
    language = models.CharField(max_length=2,choices=LANGUAGE_CHOICES, default='S')
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    @property
    def usuario(self):
        lenguage = 'English' if self.language == 'E' else 'Spanish'
        return '{} {}'.format(self.user.email, lenguage)

    def __str__(self):
        return self.user.email + ' ' + self.language