#django
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator 
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import TokenAuthentication
#rest framework
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
#firebase
from firebase_admin import auth

#serializres
from .serializers import LoginSocialSerializers, UserLanguageSerializer
#local models
from .models import User, LengauageUser

# Create your views here.


class LoginUser(TemplateView):
    template_name = "user/login.html"



#@login_required
class GoogleLoginView(APIView):
    """ validador de login desde google """
    serializers_class = LoginSocialSerializers

    def post(self, request):
        serializer = self.serializers_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        #
        token_id = serializer.data.get('token_id')
        token_decode = auth.verify_id_token(token_id)
        email = token_decode['email']
        name = token_decode['name']
        verified =token_decode['email_verified']

        usuario, created_user = User.objects.get_or_create(
            email = email,
            defaults={
             'full_name':name,
             'email':email,
             'is_active':True
            }
        )
        lang = 'S'
        user_lang, created = LengauageUser.objects.get_or_create(user = usuario)
        user_lang.language = lang
        user_lang.save()
        if created_user:
            token = Token.objects.create(user=usuario)
            login(request, usuario)
        else:
            token, createdTok =Token.objects.get_or_create(
                user=usuario
            )
            print(createdTok)
            login(request, usuario)
        response_user = {
            'id':usuario.pk,
            'email':usuario.email,
            'full_name':usuario.full_name
        }
        return Response({'token':token.key,
                         'user':response_user
                         })

class SetUserLanguage(APIView):
    """ Set user language """
    serializers_class = UserLanguageSerializer
    authentication_classes = (TokenAuthentication,)
    permission_classes = [IsAuthenticated,]

    def get(self, request):
        serializer = self.serializers_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        try:
            user_lang = LengauageUser.objects.get(user = request.user)
            lang = user_lang.language
            lang_des = 'Spanish' if lang == 'S' else 'English'

            data = {'key':lang,
                    'language': lang_des
                    }
        except NameError as e:
                data = {'status':e ,
                        'language':None
                            }
            

        return Response(data)



    def post(self, request):
        serializer = self.serializers_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        lang = serializer.data.get('language')
        user_lang, created = LengauageUser.objects.get_or_create(user = request.user)
        user_lang.language = lang
        user_lang.save()
        
        return Response({'status':'Language changed',
                         'language':lang
                         })


@login_required #debe existir una session activa
def logout_view(request):
    logout(request)
    return redirect('users:login')