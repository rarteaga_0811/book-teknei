""" Users Urls"""
from django.urls import path

from .views import LoginUser, GoogleLoginView, SetUserLanguage

from .views import logout_view

urlpatterns = [
     path(route ='login/', view = LoginUser.as_view(), name='login'),
     path(route = 'user-language', view = SetUserLanguage.as_view(), name='userlanguage'),
     path(route = 'api/google-login', view = GoogleLoginView.as_view(), name ='users-google-login'),
     #templates
     path(route = 'logout', view = logout_view, name = 'logout'),
]