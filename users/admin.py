from django.contrib import admin

# Register your models here.
from .models import LengauageUser, User

#Admin
class UserAdmin(admin.ModelAdmin):
    list_display = ['email', 'full_name','genero']

class LengauageUserAdmin(admin.ModelAdmin):
    list_display = ['usuario','language']


#Register
#admin.site.register(User, UserAdmin)
admin.site.register(LengauageUser, LengauageUserAdmin)