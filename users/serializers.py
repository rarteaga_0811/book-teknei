#rest framework
from rest_framework import serializers

#local models
from .models import User

#My serializers
class LoginSocialSerializers(serializers.Serializer):
    token_id = serializers.CharField(required=True)

class UserLanguageSerializer(serializers.Serializer):
    language = serializers.CharField()